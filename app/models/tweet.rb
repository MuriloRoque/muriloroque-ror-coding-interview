# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  belongs_to :user
  validates :body, presence: true, length: { maximum: 180 }
  validates_uniqueness_of :body, scope: :user_id, conditions: -> { where('created_at > ?', 24.hours.ago) }

  scope :non_affiliated_users, -> { joins(:user).where(users: { company_id: nil }) }
end
