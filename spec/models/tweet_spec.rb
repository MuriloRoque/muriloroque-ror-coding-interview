require 'rails_helper'

RSpec.describe Tweet, type: :model do
  it { should validate_presence_of(:body) }
  it { should validate_length_of(:body).is_at_most(180) }

  context 'body can be created' do
    let(:user) { create(:user) }
    let(:valid_body) { 'This is a valid tweet' }
    let!(:tweet1) { create(:tweet, user: user, body: valid_body) }
    it 'can create a tweet in 24 hours' do
      expect(tweet1).to be_valid
    end
  end

  context 'body can be created' do
    let(:user) { create(:user) }
    let(:valid_body) { 'This is a valid tweet' }
    let!(:tweet1) { create(:tweet, user: user, body: valid_body, created_at: 25.hours.ago) }
    let!(:tweet2) { build(:tweet, user: user, body: valid_body, created_at: 2.hours.ago) }
    it 'cannot create a tweet after 24 hours' do
      expect(tweet1).to be_valid
      expect(tweet2).to be_valid
    end
  end

  context 'body cannot be created' do
    let(:user) { create(:user) }
    let(:valid_body) { 'This is a valid tweet' }
    let!(:tweet1) { create(:tweet, user: user, body: valid_body, created_at: 2.hours.ago) }
    let!(:tweet2) { build(:tweet, user: user, body: valid_body, created_at: 2.hours.ago) }
    it 'cannot create a tweet before 24 hours' do
      expect(tweet1).to be_valid
      expect(tweet2).not_to be_valid
    end
  end
end
